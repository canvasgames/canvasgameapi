class Quad extends GameObject {

    Awake(params) {
        this.w = this.h = params.size
    }

    Draw(ctx) {

        ctx.fillRect(this.x, this.y, this.w, this.h)
        ctx.rect(this.x, this.y, this.w, this.h); 
        ctx.lineWidth = this.lineWidth.toString()
        ctx.strokeStyle = this.strokeStyle || "black"
        ctx.stroke()

    }

}