const Side = {
    left: {
        id: 0,
        coord: {
            x: -1,
            y: 0
        }
    },
    right: {
        id: 1,
        coord: {
            x: 1,
            y: 0
        }
    },
    up: {
        id: 2,
        coord: {
            x: 0,
            y: -1
        }
    },
    down: {
        id: 3,
        coord: {
            x: 0,
            y: 1
        }
    }
}